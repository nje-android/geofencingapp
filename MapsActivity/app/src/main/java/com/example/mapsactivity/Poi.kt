package com.example.mapsactivity

import com.google.android.gms.maps.model.LatLng

data class Poi(val title: String, val latLon: LatLng) {
}