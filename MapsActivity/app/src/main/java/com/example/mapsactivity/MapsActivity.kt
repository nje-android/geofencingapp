package com.example.mapsactivity

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.transition.Slide
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,
    GoogleMap.OnMapLongClickListener {

    private lateinit var mMap: GoogleMap

    private lateinit var poiList: ArrayList<Poi>

    private lateinit var markerPoi1: Marker
    private lateinit var markerPoi2: Marker
    private lateinit var markerPoi3: Marker
    private lateinit var markerPoi4: Marker
    private lateinit var markerPoi5: Marker

    private lateinit var view: View

    private lateinit var mapFragment: SupportMapFragment
    private lateinit var inflater: LayoutInflater

    private lateinit var geofencingClient: GeofencingClient
    private var FINE_LOCATION_ACCES_REQUEST_CODE = 10001;
    private var ACCESS_BACKGROUND_LOCATION = 10002;
    private var GEOFENCE_RADIUS = 50.0;
    private lateinit var geoFenceHelper: GeoFenceHelper
    private var GEOFENCE_ID = "Geofence_ID"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)


        poiList = arrayListOf<Poi>()
        poiAdding()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        view = inflater.inflate(R.layout.fragment_gamf_restaurant, null)


        //kikapcsolja a sötétmód lekérését a telefonból
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        //geofencing inicializálás
        geofencingClient = LocationServices.getGeofencingClient(this)
        geoFenceHelper = GeoFenceHelper(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        /*for (i in 0..6) {
            markerPoi1 =
                mMap.addMarker(MarkerOptions().position(poiList[i].latLon).title(poiList[i].title))
        }*/

        /*
        markerPoi1 =
            mMap.addMarker(MarkerOptions().position(poiList[0].latLon).title(poiList[0].title))
        markerPoi2 =
            mMap.addMarker(MarkerOptions().position(poiList[1].latLon).title(poiList[1].title))
        markerPoi3 =
            mMap.addMarker(MarkerOptions().position(poiList[2].latLon).title(poiList[2].title))
        markerPoi4 =
            mMap.addMarker(MarkerOptions().position(poiList[3].latLon).title(poiList[3].title))
        markerPoi5 =
            mMap.addMarker(MarkerOptions().position(poiList[4].latLon).title(poiList[4].title))
         */


        /*mMap.addPolygon(
            PolygonOptions().add(
                LatLng(46.894892, 19.668117), LatLng(46.894815, 19.667875),
                LatLng(46.894320, 19.668347), LatLng(46.894382, 19.668545)
            )
                .strokeColor(Color.RED)
        )*/

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(poiList[0].latLon, 17F))
        // mMap.animateCamera()
        mMap.setOnInfoWindowClickListener(this)
        mMap.setOnMapLongClickListener(this)

        enableUserLocation()
    }

    private fun enableUserLocation() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap.setMyLocationEnabled(true)
        } else {
            //ask on permission
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                //dialogot kell mutatni a felhasználónak
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    FINE_LOCATION_ACCES_REQUEST_CODE
                )
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    FINE_LOCATION_ACCES_REQUEST_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == FINE_LOCATION_ACCES_REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //megvan a permission
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                }
                mMap.setMyLocationEnabled(true)
            } else {
                //nincs meg a permission
            }
        }

        if (requestCode == ACCESS_BACKGROUND_LOCATION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //megvan a permission
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                mMap.setMyLocationEnabled(true)
            } else {
                //nincs meg a permission
            }
        }
    }

    fun poiAdding() {
        poiList.add(
            Poi(
                "Neumann János Egyetem GAMF Műszaki és Informatikai Kar",
                LatLng(46.896155, 19.669110)
            )
        )
        poiList.add(
            Poi(
                "Természet és Műszaki Alaptudományi Tanszék",
                LatLng(46.895783, 19.668338)
            )
        )
        poiList.add(
            Poi(
                "GAMF Étterem",
                LatLng(46.895417, 19.667219)
            )
        )
        poiList.add(
            Poi(
                "GAMF Informatikai tanszék",
                LatLng(46.894704, 19.668281)
            )
        )
        poiList.add(
            Poi(
                "GAMF könyvtár",
                LatLng(46.893998, 19.668704)
            )
        )
    }


    override fun onInfoWindowClick(p0: Marker?) {
        when (p0?.title) {
            "Neumann János Egyetem GAMF Műszaki és Informatikai Kar",
            "Természet és Műszaki Alaptudományi Tanszék",
            "GAMF Informatikai tanszék",
            "GAMF könyvtár" ->
                Toast.makeText(applicationContext, "" + p0.title, Toast.LENGTH_SHORT).show()
            "GAMF Étterem" -> {

                val popupWindow = PopupWindow(
                    view, // Custom view to show in popup window
                    LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                    LinearLayout.LayoutParams.WRAP_CONTENT // Window height
                )

                // Set an elevation for the popup window
                popupWindow.elevation = 10.0F


                // If API level 23 or higher then execute the code
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    // Create a new slide animation for popup window enter transition
                    val slideIn = Slide()
                    slideIn.slideEdge = Gravity.TOP
                    popupWindow.enterTransition = slideIn

                    // Slide animation for popup window exit transition
                    val slideOut = Slide()
                    slideOut.slideEdge = Gravity.RIGHT
                    popupWindow.exitTransition = slideOut

                }

                // Get the widgets reference from custom view
                val textViewCim: TextView = view.findViewById(R.id.textViewCim) as TextView
                val textViewNyitvatartas: TextView =
                    view.findViewById(R.id.textViewNyitvatartas) as TextView
                val textViewTelefonszam: TextView =
                    view.findViewById(R.id.textViewTelefonszam) as TextView
                val buttonBack: Button = view.findViewById(R.id.buttonBack) as Button

                val cim = "<b>" + "Cím:" + "</b>" + " Kecskemét, Izsáki út 10, 6000"
                val nyitvatartas = "<b>" + "Nyitvatartás:" + "</b>" + " 11:00-14:00"
                val telefonszam = "<b>" + "Telefonszám:" + "</b>" + " 06 30 974 3800"

                textViewCim.setText(Html.fromHtml(cim))
                textViewNyitvatartas.setText(Html.fromHtml(nyitvatartas))
                textViewTelefonszam.setText(Html.fromHtml(telefonszam))

                buttonBack.setOnClickListener {
                    Toast.makeText(applicationContext, "valami", Toast.LENGTH_SHORT).show()
                }

                supportFragmentManager.beginTransaction()
                    .replace(R.id.map, gamf_restaurant()).commit()

                // Finally, show the popup window on app
                /*TransitionManager.beginDelayedTransition(root_layout)
                popupWindow.showAtLocation(
                    root_layout, // Location to display popup window
                    Gravity.CENTER, // Exact position of layout to display popup
                    0, // X offset
                    0 // Y offset
                )*/

            }
        }

    }

    override fun onMapLongClick(p0: LatLng?) {


        if (Build.VERSION.SDK_INT >= 29) {
            //kell background permission
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                tryAddingGeofence(p0)
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    )
                ) {
                    //dialog üzenet engedélyhez
                    ActivityCompat.requestPermissions(
                        this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                        ACCESS_BACKGROUND_LOCATION
                    )
                }
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                    ACCESS_BACKGROUND_LOCATION
                )
            }
        } else {
            tryAddingGeofence(p0)
        }
    }

    private fun tryAddingGeofence(p0: LatLng?) {
        mMap.clear()
        addMarker(p0)
        addCircle(p0, GEOFENCE_RADIUS)
        addGeofence(p0, GEOFENCE_RADIUS)
    }

    private fun addGeofence(p0: LatLng?, radius: Double) {

        val geofence = geoFenceHelper.getGeofence(
            GEOFENCE_ID, p0!!, radius,
            Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_DWELL or Geofence.GEOFENCE_TRANSITION_EXIT
        )
        val pendingIntent = geoFenceHelper.pendingIntent
        val geofencingRequest = geoFenceHelper.getgeofencingRequest(geofence)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        geofencingClient.addGeofences(geofencingRequest, pendingIntent)
    }

    private fun addMarker(p0: LatLng?) {
        val markerOptions = MarkerOptions().position(p0!!)
        mMap.addMarker(markerOptions)
    }

    private fun addCircle(p0: LatLng?, radius: Double) {
        val circleOptions = CircleOptions()
        circleOptions.center(p0)
        circleOptions.radius(radius)
        circleOptions.strokeColor(Color.argb(255, 255, 0, 0))
        circleOptions.fillColor(Color.argb(64, 255, 0, 0))
        circleOptions.strokeWidth(4F)
        mMap.addCircle(circleOptions)
    }
}