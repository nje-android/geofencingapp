package com.example.mapsactivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent

class GeoFenceBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {


        val notificationHelper = NotificationHelper(context)
        val geofencingEvent = GeofencingEvent.fromIntent(intent)


        val transitionType = geofencingEvent.geofenceTransition
        when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER ->
                notificationHelper.sendHighPriorityNotification(
                    "Beléptél a zónába",
                    "Beléptél a veszélyeztetett zónába! Menekülj!!!",
                    MapsActivity::class.java
                )
            Geofence.GEOFENCE_TRANSITION_DWELL ->
                notificationHelper.sendHighPriorityNotification(
                    "A zónában mozogsz",
                    "Még életben vagy??",
                    MapsActivity::class.java
                )
            Geofence.GEOFENCE_TRANSITION_EXIT ->
                notificationHelper.sendHighPriorityNotification(
                    "Elhagytad a zónát",
                    "FUSSS!!!",
                    MapsActivity::class.java
                )
        }
    }


}